#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h> 
#include <time.h> 

int main()
{

    clock_t t; 
    int byteSize; 
    char fileName[50]; // CSE machines segfault when I use char * filename
    char buffer[1024]; // buffer to store the bytes read from file
    int bytesRead = 0;

    printf("Enter the file name: ");
    scanf("%s", fileName);

    // Open the file
    int fd_input = open(fileName, O_RDONLY | O_APPEND);
    int fd_output = open("output", O_RDWR | O_CREAT, 0777); // need the third argument to set permissions

    if(fd_input < 0) {
        printf("Input file not opened\n");
        return 0; 
    }    
    if(fd_output < 0) {
        printf("Output file not opened\n");
        return 0; 
    }

    printf("Enter the byte size to read: ");
    scanf("%d", &byteSize);

    



      
    

    t = clock();
    // Read the file and write to the other file 
    while(read(fd_input, &buffer, byteSize) > 0) {
        write(fd_output, &buffer, byteSize);
    }

    t = clock() - t; 

    if(close(fd_input) < 0)
        printf("Input not correctly closed\n");
    if(close(fd_output) < 0)
        printf("Output not correctly closed\n");

    printf("Seconds: %f\n", (float)t/CLOCKS_PER_SEC);

    return 0;
}